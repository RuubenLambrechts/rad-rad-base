const themeKit = require('@shopify/themekit');
const fs = require('fs-extra');
const path = require('path');

const folder = path.join(__dirname, './');
const targetFolder = path.join(__dirname, './../temp');

const configName = '/config/settings_data.json';
const configSrc = path.join(folder, configName);
const configTarget = path.join(targetFolder, configName);

const localesName = '/locales';
const localesSrc = path.join(folder, localesName);
const localesTarget = path.join(targetFolder, localesName);

const init = async () => {
    await themeKit.command('version');

    //await getThemes();
    await downloadTheme();
    await copyFiles();
    await newTheme();
    await deployTheme();

}


// const getThemes = async () => {

//     console.log('Fetching theme');
//     const themes = await themeKit.command('get', {
//         password: process.argv[2],
//         store: process.argv[3],
//         live: true
//     });

//     console.dir(themes);

// }


const downloadTheme = async () => {

    console.log('Downloading existing theme');
    console.dir(folder);
    console.dir(targetFolder);

    try {
    
        await downloadLive();

        const isFolderExist = fs.existsSync(path.join(folder, './assets'));
        console.log(`Folder exists ${isFolderExist}`);
        if ( !isFolderExist ){
            console.log('Should retry');
            setTimeout(async () => {
                console.log('Retrying');
                await downloadLive();
            }, 5000);
        }

    }catch(e){
        console.dir(e);
    }

}

const downloadLive = async () => {
    await themeKit.command('get', {
        password: process.argv[2],
        store: process.argv[3],
        dir: folder,
        //live: true,
        themeId: 43142053961,
        verbose: true
    });
}

const copyFiles = async() => {

    console.log('Copying all files');

    fs.copyFile( configSrc, configTarget, fs.constants.COPYFILE_FICLONE, (res) => {console.dir(res)} );
    fs.copy( localesSrc, localesTarget, {}, (res) => {console.dir(res)});

}

const newTheme = async () => {

    console.log('Creating new theme');

    try {
        await themeKit.command('new', {
            password: process.argv[2],
            store: process.argv[3],
            name: process.argv[4]
        });
    }catch(e){
        console.dir(e)
    }

}

const deployTheme = async () => {

    console.log('Deploying theme');

    try {
        await themeKit.command('deploy', {
            env: 'development',
            dir: targetFolder,
        });
    }catch(e){
        console.dir(e)
    }

}

init();

// node shopify.js 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 radikalbase.myshopify.com