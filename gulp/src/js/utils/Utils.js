import LazyLoad from "vanilla-lazyload/dist/lazyload";

export const lazyLoadImages = () => {
	var myLazyLoad = new LazyLoad({
		elements_selector: ".lazy"
	});
}
