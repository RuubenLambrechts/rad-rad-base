const fs = require('fs'),
      changed = require('gulp-changed'),
      gulp = require('gulp');

const cfg = JSON.parse(fs.readFileSync('./config.json')),
      srcFonts = cfg.paths.src.fonts,
      destFonts = cfg.paths.dest.fonts;

gulp.task('copy:fonts', () => {
  gulp.src(srcFonts)
    .pipe(changed(destFonts))
    .pipe(gulp.dest(destFonts));
});
