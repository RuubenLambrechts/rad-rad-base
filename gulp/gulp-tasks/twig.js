const fs               = require('fs'),
      gulp             = require('gulp'),
      notify           = require('gulp-notify'),
      plumber          = require('gulp-plumber'),
      twig             = require('gulp-twig-php'),
      htmlPretty       = require('gulp-html-prettify'),
      htmlMin          = require('gulp-htmlmin')
      embed            = require('gulp-embed'),
      browserSync      = require('browser-sync');

const cfg              = JSON.parse(fs.readFileSync('./config.json')),
      srcTwig          = cfg.paths.src.twig,
      destTwig         = cfg.paths.dest.html;


// Compile Twig files
gulp.task('twig', (done) => {
  gulp.src(srcTwig)
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(twig())
    .pipe(htmlMin({collapseWhitespace: true}))
    .pipe(htmlPretty({indent_char: ' ', indent_size: 2}))
    .pipe(gulp.dest(destTwig))
    .on('end', () => { done(); });
});
