const fs = require('fs'),
      path = require('path');
      gulp = require('gulp'),
      notify = require('gulp-notify'),
      plumber = require('gulp-plumber'),
      gulpWebpack = require('webpack-stream'),
      webpack = require('webpack'),
      uglify = require('gulp-uglify'),
      pkg = require('../package.json'),
      SourceMapDevToolPlugin = require('webpack/lib/SourceMapDevToolPlugin'),
      minimist = require('minimist');

const cfg = JSON.parse(fs.readFileSync('./config.json')),
      srcScripts = cfg.paths.src.js,
      srcScriptsLib = cfg.paths.src.js_libs,
      destScripts = cfg.paths.dest.js;


var knownOptions = {
  string: 'env',
  default: { env: process.env.NODE_ENV || 'development' }
};

var options = minimist(process.argv.slice(2), knownOptions);

// JS Dev Task
gulp.task('scripts', () => {
  gulp.src(srcScripts)
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(gulpWebpack({
      entry: {
        //# Multiple entry points
        //# -------------------------------------------------------
        //# https://webpack.js.org/concepts/entry-points/
        //# -------------------------------------------------------
        "app": srcScripts,
        "vendor": ['jquery', 'enquire.js', 'vanilla-lazyload/dist/lazyload']
      },

      output: {
        filename: '[name].bundle.js',
        // library: 'EntryPoint'
      },

      watch: (options.env === 'development') ? true : false,
      module: {
        loaders: [
          //# Get your modernizr build bundled with webpack
          //# -------------------------------------------------------
          //# https://www.npmjs.com/package/modernizr-loader
          //# -------------------------------------------------------
          // {
          //   test: /modernizr.config.json$/,
          //   loader: "modernizr-loader!json-loader"
          // },

          {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            loader: 'babel-loader',
            options: {
              presets: [
                ['env', { modules: false }]
              ]
            },
          },
        ],
      },


      resolve: {
        alias: {
          //# Create aliases to import or require certain modules more easily.
          //# -------------------------------------------------------
          //# https://webpack.js.org/configuration/resolve/#resolve-alias
          //# -------------------------------------------------------
          // jquery: 'jquery/src/jquery',
          // jqueryui: './src/js/vendors/jquery-ui',
          // slick: 'slick-carousel',
          // modernizr$: path.resolve(__dirname, '../modernizr.config.json'),
        },
      },


      //# Externals
      //# The externals configuration option provides a way of excluding dependencies
      //# from the output bundles. Instead, the created bundle relies on that dependency
      //# to be present in the consumer's environment. This feature is typically most
      //# useful to library developers, however there are a variety of applications for it.
      //# -------------------------------------------------------
      //# https://webpack.js.org/configuration/externals/
      //# -------------------------------------------------------
      externals: {
        // jquery: 'jQuery'
      },


      plugins: [
        //# The CommonsChunkPlugin is an opt-in feature that creates a separate file (known as a chunk), consisting of
        //# common modules shared between multiple entry points.
        //# -------------------------------------------------------
        //# By separating common modules from bundles, the resulting chunked file can be loaded once initially,
        //# and stored in cache for later use. This results in page speed optimizations as the browser can quickly
        //# serve the shared code from cache, rather than being forced to load a larger bundle whenever a new page is visited.
        //# -------------------------------------------------------
        // https://webpack.js.org/plugins/commons-chunk-plugin/#src/components/Sidebar/Sidebar.jsx
        //# -------------------------------------------------------
        new webpack.optimize.CommonsChunkPlugin({
          name: 'vendor',
          filename: 'vendor.bundle.js'
        }),


        //# Automatically load modules instead of having to import or require them everywhere.
        //# -------------------------------------------------------
        //# Whenever the identifier is encountered as free variable in a module, the module is
        //# loaded automatically and the identifier is filled with the exports of the loaded module
        //# (or property in order to support named exports).
        //# -------------------------------------------------------
        //# https://webpack.js.org/plugins/provide-plugin/
        //# -------------------------------------------------------
        new webpack.ProvidePlugin({
          $: 'jquery',
          jQuery: 'jquery',
          // slick: 'slick-carousel',
          LazyLoad: 'vanilla-lazyload/dist/lazyload',
          enquire: 'enquire.js'
        }),


        //# This plugin enables more fine grained control of source map generation. It is also enabled
        //# automatically by certain settings of the devtool configuration option.
        //# -------------------------------------------------------
        //# https://webpack.js.org/plugins/source-map-dev-tool-plugin/
        //# -------------------------------------------------------
        new webpack.SourceMapDevToolPlugin({
          filename: '[file].map',
          exclude: ['vendor.bundle.js']
        }),

        new webpack.optimize.UglifyJsPlugin({
          sourceMap: true,
          compress: {
            dead_code: true,
            unused: true,
            warnings: false,
            screw_ie8 : true
          },
          mangle: {
            screw_ie8: true,
            keep_fnames: true
          },
          output: {
            comments: false
          }
        }),
      ]
    }, webpack))
    .pipe(gulp.dest(destScripts));
});
